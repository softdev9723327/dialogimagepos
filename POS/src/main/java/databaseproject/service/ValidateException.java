/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package databaseproject.service;

/**
 *
 * @author asus
 */
public class ValidateException extends Exception {
    public ValidateException(String message) {
        super(message);
    }

}
